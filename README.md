# Awesome Eurovision

My favourite Eurovision performances


## 2023

🇵🇸


## 2022

[🇷🇴](https://www.youtube.com/watch?v=nPpuwy79sHs)
[🇫🇷](https://www.youtube.com/watch?v=H1lcGXwOqJI)
[🇳🇴](https://www.youtube.com/watch?v=adCU2rQyDeY)
[🇱🇻](https://www.youtube.com/watch?v=TM0_0WfuxSk)
[🇦🇺](https://www.youtube.com/watch?v=wosfFz2FJPU)


## 2021

[🇮🇸](https://www.youtube.com/watch?v=YSMhu-PrLME)
[🇩🇪](https://www.youtube.com/watch?v=1m0VEAfLV4E)
[🇫🇷](https://www.youtube.com/watch?v=Unj9WbeLzRU)


## 2020

[🇮🇸](https://www.youtube.com/watch?v=1HU7ocv3S2o)


## 2019

[🇦🇺](https://www.youtube.com/watch?v=Eo5H62mCIsg)
[🇫🇷](https://www.youtube.com/watch?v=AjZNbIQtWvw)
[🇮🇸](https://www.youtube.com/watch?v=kTb69WkBbvs)


## 2018

[🇪🇪](https://www.youtube.com/watch?v=ImawXdXIGd8)
[🇲🇩](https://www.youtube.com/watch?v=5evrVL1rJPA)


## 2017

[🇬🇧](https://www.youtube.com/watch?v=Fl1GYTg4GmA)


## 2016

[🇷🇺](https://www.youtube.com/watch?v=e94dst20C9Y)
[💥](https://www.youtube.com/watch?v=Cv6tgnx6jTQ)


## 2015

[🇦🇺](https://www.youtube.com/watch?v=H0EhhZWXTng)
[🇱🇻](https://www.youtube.com/watch?v=-usdXbeGHi8)
[🇸🇪](https://www.youtube.com/watch?v=5sGOwFVUU0I)
[🇷🇴](https://www.youtube.com/watch?v=BGw0tUKOW7o)
